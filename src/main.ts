import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { JwtService } from '@nestjs/jwt';
import { ValidationPipe } from '@nestjs/common';

(async function () {
  const PORT = process.env.PORT || 3010;
  const app = await NestFactory.create(AppModule);

  const config = new DocumentBuilder()
    .setTitle('First full cycle nestjs application')
    .setDescription('rest api docs')
    .setVersion('0.0.1')
    .addTag('Nika')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);

  // app.useGlobalGuards(new JwtAuthGuard());
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
  });
})();
