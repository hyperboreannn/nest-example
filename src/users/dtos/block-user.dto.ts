import { ApiProperty } from '@nestjs/swagger';

export class BlockUserDto {
  @ApiProperty({ example: 'some@example.com', description: 'User email' })
  readonly userId: number;
  @ApiProperty({ example: '123456', description: 'User password' })
  readonly blockReason: string;
}
