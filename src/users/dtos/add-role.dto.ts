import { ApiProperty } from '@nestjs/swagger';

export class AddRoleDto {
  @ApiProperty({ example: 'some@example.com', description: 'User email' })
  readonly value: string;
  @ApiProperty({ example: '123456', description: 'User password' })
  readonly userId: number;
}
